package no.experis.academy;

import java.sql.*;


public class Main {

    public static void main(String[] args) {
        String sql = "SELECT * FROM employee";
        try {
            Connection conn = connect();
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery(sql);
            while (rs.next()){
                System.out.println(rs.getString("firstname"));
                System.out.println(rs.getString("title"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static Connection connect() {
        Connection conn = null;
        try {
            String url = "jdbc:sqlite:resources/Northwind_small.sqlite";
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } return conn;

        /*finally {
            try {
                if (conn != null){
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }*/
    }
}
